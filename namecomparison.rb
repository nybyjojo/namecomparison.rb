# A program to compare two CSV lists 
# and return data that is on both lists

# Files must be present in the directory the
# program is in and the two files must
# match the naming conventions in the program

# open an array for each list of names
people = Array.new
people1 = Array.new

# load each list into a variable
first_list = File.open("namelist.csv", "r")
second_list = File.open("namecomplist.csv", "r")

# split the file up by line and push
# the contents of the line to an array
# each element is delineated by the new line
# character "\n"
first_list.each_line { |line|
	fields = line.split('\n')
	people.push(fields)
}

# split the second file up by line and push
# the contents of the line to an array
second_list.each_line { |line|
	fields = line.split('\n')
	people1.push(fields)
}

# There has to be a cleaner way to do the folowing
# Put the data that is in both files in a variable
final_list = people & people1

# convert the array to a string by joing it
final_list1 = final_list.join

# print the matching data and substitute a comma
# for a space for formatting purposes
# the result should be the names of the people
# who are on both lists
puts final_list1.gsub(',', ' ')